// Answer key for a word search grid
// Usage > node .\wordsearch.js input.txt
//-----------------------------------------
const fs = require('fs');
const ReadLines = require('n-readlines');

if (process.argv.length < 3) {
    console.log("Usage: node wordsearch.js input.txt");
    return;
}
const readLines = new ReadLines(process.argv[2]);

let ROWS; //grid size
let COLUMNS; //grid size
//For found word
let startIndex = ""; //Format as "row:col"
let endIndex = ""; //Format as "row:col"
//These are for easily pointing in a new direction to search
const xDirections = [-1, -1, -1, 0, 0, 1, 1, 1];
const yDirections = [-1, 0, 1, -1, 1, -1, 0, 1];

/*
Main word search program
*/
function main() {
    //Read the grid size from the file
    let firstline = readLines.next().toString().split('x');
    ROWS = parseInt(firstline[0]);
    COLUMNS = parseInt(firstline[1]);

    //Read the character grid from the file line-by-line
    let grid = new Array(ROWS);
    for (var i = 0; i < ROWS; i++) {
        grid[i] = readLines.next().toString().trim().split(" ");
    }

    //Read each of the words and pass them to search()
    let line;
    while ((line = readLines.next())) {
        // console.log(line.toString('ascii'));
        search(grid, line.toString().trim());
    }
}

/*
Method to search for a given string keyWord in grid
Prints startIndex and endIndex of found string
*/
function search(grid, keyWord) {
    //Look for first char of keyWord at each point
    for (var row = 0; row < ROWS; row++) {
        for (var col = 0; col < COLUMNS; col++) {
            //If the first char matches, search for word in all 8 directions and print the indices
            if (grid[row][col] === keyWord.charAt(0) && searchLine(grid, keyWord, row, col)) {
                console.log(keyWord + " " + startIndex + " " + endIndex);
            }
        }
    }
}

/*
Method to search in all 8 directions of grid starting at the point specified
Returns true if keyWord is found; false otherwise
Sets startIndex and endIndex to the indices of found keyWord
*/
function searchLine(grid, keyWord, row, col) {

    //Since we know first char matches, match remaining chars
    for (var direction = 0; direction < 8; direction++) {
        let nextRow = row + xDirections[direction];
        let nextCol = col + yDirections[direction];

        let i; //need to pull this out to access it on line 78
        for (i = 1; i < keyWord.length; i++) {

            //If out of bounds then we did not find the word;
            //break and search different direction
            if (nextRow >= ROWS || nextRow < 0 || nextCol >= COLUMNS || nextCol < 0)
                break;
            //If char does not match then we did not find the word;
            //break and search different direction
            if (grid[nextRow][nextCol] !== keyWord.charAt(i))
                break;

            endIndex = nextRow + ":" + nextCol;

            //Moving in direction of matched char
            nextRow += xDirections[direction];
            nextCol += yDirections[direction];
        }
        //Found word only if above loop finished
        if (i == keyWord.length) {
            startIndex = row + ":" + col;
            return true;
        }
    }
    //Did not find word
    return false;
}

//run the program
main();